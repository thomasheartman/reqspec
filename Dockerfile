FROM rust:1.48 as build

WORKDIR /build

COPY . .

RUN cargo install --path .

######## DINKUS ########

FROM debian:buster-slim

ENV port=8080

LABEL io.openshift.expose-services="${port}:http" \
        io.openshift.tags=rust \
        io.openshift.description="A request inspector. Responds with data about the incoming request."


COPY --from=build /usr/local/cargo/bin/reqspec .

EXPOSE ${port}

ENTRYPOINT ["./reqspec"]
