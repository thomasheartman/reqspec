{ pkgs ? import <nixpkgs> {
  overlays = [
    (import (builtins.fetchTarball
      "https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz"))
  ];
} }:
with pkgs;
let

  rust = pkgs.latest.rustChannels.stable.rust.override {
    extensions = [ "rust-src" ];
    targets = [ "x86_64-unknown-linux-musl"];
  };

in pkgs.mkShell {
  buildInputs = with pkgs; [ cargo-watch rust rust-analyzer cargo-edit ];
}
