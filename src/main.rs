mod program;

use clap::Clap;
use tracing::{info, instrument, Level};
use std::env;
use tracing_subscriber::FmtSubscriber;
use std::str::FromStr;

#[derive(Clap)]
#[clap(version = "0.1", author = "Thomas Heartman <thomasheartman@gmail.com>")]
struct Opts {
    #[clap(short, long, about = "The port to run the application on.")]
    port: Option<u16>,

    #[clap(short, long, about = r#"The minimum log level for the application. Valid values are one of "error", "warn", "info", "debug", "trace", or a number 1-5."#)]
    min_log_level: Option<Level>,
}

fn option_fallback<T>(command_provided: Option<T>, env_var_name: &str, fallback: T) -> T where T : FromStr {
    command_provided.or(env::var(env_var_name).ok().as_ref().and_then(|s|  T::from_str(s).ok())).unwrap_or(fallback)
}


#[actix_rt::main]
#[instrument]
async fn main() -> std::io::Result<()> {
    let opts = Opts::parse();

    let min_log_level = option_fallback(opts.min_log_level, "REQSPEC_MIN_LOG_LEVEL", Level::INFO);
    let port = option_fallback(opts.port, "REQSPEC_PORT", 8080);

    let min_log_level_str = min_log_level.to_string();

    let subscriber = FmtSubscriber::builder()
        .with_max_level(min_log_level)
        .finish();
    tracing::subscriber::set_global_default(subscriber).expect("setting_default_subscriber_failed");

    info!("Starting application");
    info!("Using minimum log level {}", min_log_level_str);
    info!("Listening on port {}", port);

    program::start(port).await
}
