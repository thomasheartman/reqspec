use actix_web::{
    guard, http::header::HeaderMap, web, App, HttpRequest, HttpResponse, HttpServer, Responder,
};
use serde::Serialize;
use serde_json::json;
use std::collections::HashMap;
use tracing::{info, instrument};

fn to_string_map(headers: &HeaderMap) -> HashMap<&str, Vec<String>> {
    let mut map: HashMap<_, Vec<_>> = HashMap::new();
    for (header_name, header_value) in headers.iter() {
        let k = header_name.as_str();
        let v = String::from_utf8_lossy(header_value.as_bytes()).into();

        map.entry(k).or_default().push(v);
    }

    map
}

#[derive(Serialize, Debug)]
struct QueryStringData<'a> {
    raw: &'a str,
    parsed: HashMap<&'a str, Vec<&'a str>>,
}

impl<'a> QueryStringData<'a> {
    fn from_raw_query_string(query_string: &str) -> Option<QueryStringData> {
        match query_string.len() {
            0 => None,
            _ => {
                let pairs = query_string.split("&");
                let mut parsed = HashMap::<_, Vec<&str>>::new();
                for (k, v) in pairs.filter_map(|s| s.find("=").map(|i| s.split_at(i))) {
                    parsed.entry(k).or_default().push(&v[1..])
                }

                Some(QueryStringData {
                    raw: query_string,
                    parsed,
                })
            }
        }
    }
}

#[derive(Serialize, Debug)]
#[serde(rename_all = "camelCase")]
struct Response<'a, Body>
where
    Body: serde::ser::Serialize + std::fmt::Debug,
{
    headers: HashMap<&'a str, Vec<String>>,
    path: &'a str,
    query_string: Option<QueryStringData<'a>>,
    method: &'a str,
    version: String,
    body: &'a Body,
}

#[instrument]
fn handle_request_base(
    req: HttpRequest,
    body: impl serde::ser::Serialize + std::fmt::Debug,
) -> impl Responder {
    let response = Response {
        headers: to_string_map(req.headers()),
        path: req.path(),
        query_string: QueryStringData::from_raw_query_string(req.query_string()),
        method: req.method().as_str(),
        version: format!("{:?}", req.version()),
        body: &body,
    };

    info!("{:?}", response);

    HttpResponse::Ok()
        .content_type("application/json")
        .body(json!(response))
}

async fn handle_json_request_with_body(
    req: HttpRequest,
    body_raw: web::Json<serde_json::Value>,
) -> impl Responder {
    handle_request_base(req, body_raw.into_inner())
}

async fn handle_request(req: HttpRequest) -> impl Responder {
    handle_request_base(
        req,
        "The request either had no body or had an unsupported format.",
    )
}

pub(crate) async fn start(port: u16) -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(
                web::resource("/{path:.*}")
                    .guard(guard::fn_guard(|req| {
                        req.headers
                            .get("content-length")
                            .map(|v| v != "0")
                            .unwrap_or(false)
                    }))
                    .guard(guard::Header("content-type", "application/json"))
                    .to(handle_json_request_with_body),
            )
            .default_service(web::route().to(handle_request))
    })
    .bind(format!("0.0.0.0:{}", port))?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn query_string_data_serializes_multiple_entries_to_same_key_correctly() {
        let input = "a=foo&a=bar";
        let result = QueryStringData::from_raw_query_string(input);
        println!("Result: {:?}", result);
        match result.and_then(|m| m.parsed.get("a").map(|v| v.clone())) {
            Some(vec) => {
                assert!(vec.contains(&"foo"));
                assert!(vec.contains(&"bar"));
            }
            None => assert!(false),
        }
    }

    #[test]
    fn query_string_data_returns_none_if_len_is_0() {
        let input = "";

        assert!(QueryStringData::from_raw_query_string(input).is_none());
    }
}
